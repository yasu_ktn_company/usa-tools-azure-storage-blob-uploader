declare module "azure-storage" {
  export interface BlobService {
    createContainerIfNotExists(container: string,
      options: any, callback: Function): void;
    createBlockBlobFromLocalFile(container: string,
      blogName: string, localFilePath: string, callback: Function): void;
  }
  export function createBlobService(account: string, accountKey: string): BlobService;
}
