/// <reference path="../typings/main.d.ts" />
/// <reference path="./typings/main.d.ts" />

import * as fs from "fs";
import * as async from "async";
import * as azure from "azure-storage";

interface AzureStorageConfig {
  account: string;
  accountKey: string;
  container: string;
};

class App {
  private static config: AzureStorageConfig =
    JSON.parse(fs.readFileSync("azure-storage-config.json").toString());
  private static blobService =
    azure.createBlobService(App.config.account, App.config.accountKey);

  static run = () => {
    async.waterfall(
      [
        (callback: Function): void => {
          // コンテナ生成
          App.createContainerIfNotExists(callback);
        },
        (callback: Function):void => {
          // ファイルアップロード
          App.createBlockBlobFromLocalFile(callback);
        }
      ]
    );
  }

  private static createContainerIfNotExists = (callback: Function): void => {
    App.blobService.createContainerIfNotExists(App.config.container, {
      publicAccessLevel: "blob"
    }, (error, result, response) => {
      if (!error) {
        if (result) {
          console.log("created container.");
        } else {
          // console.log("already created container.");
        }
        callback(null);
      } else {
        console.log("failed create container.");
      }
    });
  }

  private static createBlockBlobFromLocalFile = (callback: Function): void => {
    var uploadDir = "contents/upload/";
    var uploadedDir = "contents/uploaded/";
    var files = fs.readdirSync(uploadDir);
    files.forEach((file) => {
      App.blobService.createBlockBlobFromLocalFile(App.config.container,
        file, uploadDir + file, (error, result, response) => {
        if (!error) {
          // アップロードに成功したファイルをuploadedフォルダに移動
          fs.renameSync(uploadDir + file, uploadedDir + file);
          console.log("uploaded: " + file);
        }
      });
    });
  }
}
App.run();
