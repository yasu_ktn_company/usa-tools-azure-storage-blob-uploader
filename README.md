# usa-tools-azure-storage-blob-uploader #

File Uploader to Azure Blob Storage. To work with Windows and Mac (and maybe Linux...)

## Description ##
This tool upload binary data from local file to Azure Blob Storage.

## How do I get set up? ##

### Install node.js ###
Get Installer https://nodejs.org/en/

I know, to work with the next version.

* nodev0.12.7
* nodev5.6.0
* nodev5.7.1

### Install node modules "gulp" and "typings" to global ###
```
#!sh

npm install -g gulp
npm install -g typings
```

I know, to work with the next version.

* gulp@3.9.0 or gulp@3.9.1
* typings@0.7.3

### Clone this repository ###
```
#!sh

mkdir usa-tools-azure-storage-blob-uploader
cd usa-tools-azure-storage-blob-uploader
git clone https://yasu_ktn_company@bitbucket.org/yasu_ktn_company/usa-tools-azure-storage-blob-uploader.git .
```

### Install node modules and declaration files (*.d.ts) to local ###
```
#!sh

npm install
typings install
```

### Setup execution environment ###
```
#!sh

gulp
```

### Edit azure-storage-config.json ###
When using this tool, you must provide connection information for the storage account to use.

```
#!sh

# azure-storage-config.json
{
  "account": "set_your_account",
  "accountKey": "set_your_accountKey",
  "container": "set_your_container"
}
```

## How to Use ##

### Put the file in the "contents/upload" folder ###
Put the file in the "contents/upload" folder.

### Execute "gulp run" ###
Execute gulp task.

```
#!sh

gulp run
```

Then, upload files in the "contents/upload" folder, and are moved to "contents/uploaded" folder.